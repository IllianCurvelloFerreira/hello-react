import HelloWorld from "./components/HelloWorld/HelloWorld.jsx";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HelloWorld />
      </header>
    </div>
  );
}

export default App;
